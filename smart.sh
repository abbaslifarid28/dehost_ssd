export name=$(smartctl -i /dev/sda|grep "Serial Number"|awk '{print $3}')
export eman=$(smartctl -i /dev/sdb|grep "Serial Number"|awk '{print $3}')
export mane=$(smartctl -i /dev/sdc|grep "Serial Number"|awk '{print $3}')
export anem=$(smartctl -i /dev/sdd|grep "Serial Number"|awk '{print $3}')
export mena=$(smartctl -i /dev/sde|grep "Serial Number"|awk '{print $3}')
export amen=$(smartctl -i /dev/sdf|grep "Serial Number"|awk '{print $3}')
export mean=$(smartctl -i /dev/sdg|grep "Serial Number"|awk '{print $3}')
export mnea=$(smartctl -i /dev/sdh|grep "Serial Number"|awk '{print $3}')

rm -rf usr/lib/telegraf/scripts/sda*
rm -rf /etc/telegraf/telegraf.d/ssd.conf

mv sda.sh /usr/lib/telegraf/scripts/
mv sdb.sh /usr/lib/telegraf/scripts/
mv sdc.sh /usr/lib/telegraf/scripts/
mv sdd.sh /usr/lib/telegraf/scripts/
mv sde.sh /usr/lib/telegraf/scripts/
mv sdf.sh /usr/lib/telegraf/scripts/
mv sdg.sh /usr/lib/telegraf/scripts/
mv sdh.sh /usr/lib/telegraf/scripts/
mv ssd.conf /etc/telegraf/telegraf.d/

sed -i  "3 s/ad/$name/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "11 s/ad/$eman/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "19 s/ad/$mane/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "27 s/ad/$anem/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "35 s/ad/$mena/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "43 s/ad/$amen/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "51 s/ad/$mean/" /etc/telegraf/telegraf.d/ssd.conf

sed -i  "59 s/ad/$mnea/" /etc/telegraf/telegraf.d/ssd.conf



sed -i '28 s/interval = "10s"/interval = "200s"/' /etc/telegraf/telegraf.conf|sed -i  '56 s/flush_interval = "10s"/flush_interval = "200s"/' /etc/telegraf/telegraf.conf
sudo chmod u+s /usr/sbin/smartctl
systemctl restart telegraf
systemctl restart pvestatd
